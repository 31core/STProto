package datapack

const (
	NULL_DATA = 0
	BINARY    = 1
	ASCII     = 2
	ANSI      = 3
	UTF_8     = 4
	UTF_16LE  = 5
	UTF_16BE  = 6
	GBK       = 7
	GB_2312   = 8
	GB_18030  = 9
	SHIFTJS   = 10
)
