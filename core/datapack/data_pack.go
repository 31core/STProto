package datapack

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"time"
)

const VERSION = 1

const HEADER_SIZE = 47

type DataPack struct {
	Version   uint8
	Method    uint8
	TimeStamp int64
	Encoding  uint8
	SHA256    [sha256.Size]byte
	Size      uint32
	Data      []byte
}

func New() DataPack {
	var pack DataPack
	pack.Version = VERSION
	return pack
}

/* build datapack */
func (self *DataPack) Build() []byte {
	self.SHA256 = sha256.Sum256(self.Data)
	self.Size = uint32(len(self.Data))
	self.TimeStamp = time.Now().Unix()

	buffer := bytes.NewBuffer([]byte{})
	binary.Write(buffer, binary.BigEndian, self.Version)
	binary.Write(buffer, binary.BigEndian, self.Method)
	binary.Write(buffer, binary.BigEndian, self.TimeStamp)
	binary.Write(buffer, binary.BigEndian, self.Encoding)
	binary.Write(buffer, binary.BigEndian, self.SHA256)
	binary.Write(buffer, binary.BigEndian, self.Size)
	return append(buffer.Bytes(), self.Data...)
}

func (self *DataPack) Parse(data []byte) {
	buffer := bytes.NewBuffer(data)
	binary.Read(buffer, binary.BigEndian, &self.Version)
	binary.Read(buffer, binary.BigEndian, &self.Method)
	binary.Read(buffer, binary.BigEndian, &self.TimeStamp)
	binary.Read(buffer, binary.BigEndian, &self.Encoding)
	binary.Read(buffer, binary.BigEndian, &self.SHA256)
	binary.Read(buffer, binary.BigEndian, &self.Size)
}

func (self *DataPack) CheckSum() bool {
	sum := sha256.Sum256(self.Data)
	return sum == self.SHA256
}

func (self *DataPack) Cleanup() {
	self.Method = METHOD_REGULAR
	self.Encoding = NULL_DATA
	self.Data = []byte{}
}
