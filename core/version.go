package core

const (
	PROTO_VERSION        = 1
	CLIENT_VERSION_MAJOR = 1
	CLIENT_VERSION_MINOR = 0
)

type VersionData struct {
	ProtoVersion  uint8
	ClientVersion [2]uint8
}

func NewVersionData() VersionData {
	var version_data VersionData
	version_data.ProtoVersion = PROTO_VERSION
	version_data.ClientVersion = [2]uint8{CLIENT_VERSION_MAJOR, CLIENT_VERSION_MINOR}
	return version_data
}
