package crypto

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
)

func GenerateRSAKey() ([]byte, []byte) {
	privateKey, _ := rsa.GenerateKey(rand.Reader, 2048)
	private := x509.MarshalPKCS1PrivateKey(privateKey)
	public := x509.MarshalPKCS1PublicKey(&privateKey.PublicKey)
	return private, public
}

func RSAEncrypt(pub_key_x509, data []byte) []byte {
	pub_key, _ := x509.ParsePKCS1PublicKey(pub_key_x509)
	cipher, _ := rsa.EncryptPKCS1v15(rand.Reader, pub_key, data)
	return cipher
}

func RSADecrypt(pri_key_x509, data []byte) []byte {
	pri_key, _ := x509.ParsePKCS1PrivateKey(pri_key_x509)
	plain, _ := rsa.DecryptPKCS1v15(rand.Reader, pri_key, data)
	return plain
}
