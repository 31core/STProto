package crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
)

func pkcs7_padding(data []byte) []byte {
	padding := aes.BlockSize - len(data)%aes.BlockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padText...)
}

func pkcs7_unpadding(data []byte) []byte {
	length := len(data)
	unPadding := int(data[length-1])
	return data[:(length - unPadding)]
}

func AESEncrypt(key [aes.BlockSize]byte, data []byte) []byte {
	cryptor, _ := aes.NewCipher(key[:])
	data = pkcs7_padding(data)
	ciphertext := make([]byte, len(data))
	block_cryptor := cipher.NewCBCEncrypter(cryptor, key[:])
	block_cryptor.CryptBlocks(ciphertext, data)
	return ciphertext
}

func AesDecrypt(key [aes.BlockSize]byte, data []byte) []byte {
	cryptor, _ := aes.NewCipher(key[:])
	block_cryptor := cipher.NewCBCDecrypter(cryptor, key[:])

	plain := make([]byte, len(data))
	block_cryptor.CryptBlocks(plain, data)
	plain = pkcs7_unpadding(plain)
	return plain
}
