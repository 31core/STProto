package core

import (
	"STProto/core/crypto"
	"STProto/core/datapack"
	"bytes"
	"crypto/aes"
	"encoding/binary"
	"fmt"
	"math/rand"
	"net"
	"time"
)

func (self *Connection) Accept() error {
	server, err := net.Listen("tcp", fmt.Sprintf("%s:%d", self.Host, self.Port))
	if err != nil {
		return err
	}
	self.Connection, err = server.Accept()
	if err != nil {
		return err
	}

	/* send server protocol version */
	version_data := NewVersionData()
	vd := bytes.NewBuffer([]byte{})
	binary.Write(vd, binary.BigEndian, version_data.ProtoVersion)
	binary.Write(vd, binary.BigEndian, version_data.ClientVersion)
	self.DataPack.Method = datapack.METHOD_HANDSHAKE
	self.DataPack.Data = vd.Bytes()
	self.RawSend()

	/* receive RSA public key from client */
	err = self.RawReceive()
	if err != nil {
		return err
	}
	pub_key := self.DataPack.Data
	/* prepare AES128 key */
	var aes_key [aes.BlockSize]byte
	rand.Read(aes_key[:])
	self.DataPack.Method = datapack.METHOD_HANDSHAKE
	self.DataPack.Data = crypto.RSAEncrypt(pub_key, aes_key[:])
	err = self.RawSend()
	if err != nil {
		return err
	}
	self.AESKey = aes_key
	self.TimeStamp = time.Now().Unix()
	return nil
}
