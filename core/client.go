package core

import (
	"STProto/core/crypto"
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"time"
)

func (self *Connection) Connect() error {
	var err error
	self.Connection, err = net.Dial("tcp", fmt.Sprintf("%s:%d", self.Host, self.Port))
	if err != nil {
		return err
	}

	/* verify server protocol version */
	self.RawReceive()
	version_data := NewVersionData()
	vd := bytes.NewBuffer(self.DataPack.Data)
	binary.Read(vd, binary.BigEndian, &version_data.ProtoVersion)
	binary.Read(vd, binary.BigEndian, &version_data.ClientVersion)

	if version_data.ProtoVersion != PROTO_VERSION {
		self.Close()
		return errors.New("protocol version does not match")
	}

	/* send RSA public key to server */
	pri_key, pub_key := crypto.GenerateRSAKey()
	self.DataPack.Data = pub_key
	self.RawSend()
	/* receive AES key from server */
	self.RawReceive()
	copy(self.AESKey[:], crypto.RSADecrypt(pri_key, self.DataPack.Data))

	self.TimeStamp = time.Now().Unix()
	return nil
}
