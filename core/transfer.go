package core

import (
	"STProto/core/crypto"
	"STProto/core/datapack"
	"crypto/aes"
	"errors"
	"net"
)

const DEFAULT_PORT = 5765

type Connection struct {
	Host      string
	Port      uint16
	AESKey    [aes.BlockSize]byte
	TimeStamp int64
	DataPack  *datapack.DataPack

	Connection net.Conn
}

func NewConnection(addr string, port uint16) Connection {
	var conn Connection
	pack := datapack.New()
	conn.DataPack = &pack
	conn.Host = addr
	conn.Port = port
	return conn
}

func (self *Connection) Send() error {
	self.DataPack.Data = crypto.AESEncrypt(self.AESKey, self.DataPack.Data)
	return self.RawSend()
}

/* send without encryption */
func (self *Connection) RawSend() error {
	data := self.DataPack.Build()
	_, err := self.Connection.Write(data)
	self.DataPack.Cleanup()
	return err
}

func (self *Connection) Receive() error {
	err := self.RawReceive()
	if err != nil {
		return err
	}
	self.DataPack.Data = crypto.AesDecrypt(self.AESKey, self.DataPack.Data)
	if !self.DataPack.CheckSum() {
		return errors.New("sha-256 checksum fault")
	}
	return nil
}

/* receive without encryption */
func (self *Connection) RawReceive() error {
	/* read data header */
	header := make([]byte, datapack.HEADER_SIZE)
	_, err := self.Connection.Read(header)
	if err != nil {
		return err
	}

	self.DataPack.Parse(header)
	/* read data body */
	body := make([]byte, self.DataPack.Size)
	_, err = self.Connection.Read(body)
	if err != nil {
		return err
	}
	self.DataPack.Data = body
	return err
}

func (self *Connection) Close() {
	self.Connection.Close()
}
