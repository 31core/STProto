import ctypes

DEFAULT_PORT = 5765

api = ctypes.CDLL("./libSTProto.so")

class STProto:
    def __init__(self, host: str, port: int) -> None:
        self.conn = api.STNewConnection(ctypes.c_char_p(host.encode()), 5765)
    def Accept(self) -> None:
        """STAccept of C library"""
        if api.STAccept(self.conn) < 0:
            raise IOError("couldn't accept")
    def Connect(self) -> None:
        """STConnect of C library"""
        if api.STConnect(self.conn) < 0:
            raise IOError("couldn't connect")
    def SendData(self, data: bytes) -> None:
        """STSendData of C library"""
        if api.STSendData(self.conn, ctypes.c_char_p(data), len(data)) < 0:
            raise IOError("couldn't send")
    def ReceiveData(self) -> bytes:
        """STReceiveData of C library"""
        if api.STReceive(self.conn) < 0:
            raise IOError("couldn't receive")
        len = api.STDataPackGetSize(self.conn)
        buf = ctypes.create_string_buffer(len * " ".encode())
        api.STDataPackGetData(self.conn, buf, len)
        return buf.value
    def Close(self) -> None:
        """STClose of C library"""
        if api.STClose(self.conn) < 0:
            raise IOError("couldn't close")
