#include <stdint.h>
#include <stddef.h>

#define DEFAULT_PORT 5765

typedef int stconn_t;

stconn_t STNewConnection(const char *, uint16_t);
int STAccept(stconn_t);
int STConnect(stconn_t);
int STSendData(stconn_t, char *, size_t);
int STReceive(stconn_t);
size_t STReceiveData(stconn_t, char *);
size_t STDataPackGetSize(stconn_t);
int STDataPackGetData(stconn_t, char *);
void STClose(stconn_t);
