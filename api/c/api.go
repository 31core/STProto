package c

import (
	"STProto/core"
	"unsafe"
)

/*
#include <string.h>
*/
import "C"

var conns []*core.Connection

//export STNewConnection
func STNewConnection(addr *C.char, port C.short) C.int {
	conn := core.NewConnection(C.GoString(addr), uint16(port))
	conn.Host = C.GoString(addr)
	conn.Port = uint16(port)
	conns = append(conns, &conn)
	return C.int(len(conns) - 1)
}

//export STAccept
func STAccept(c_conn C.int) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	if conn.Accept() != nil {
		return -1
	}
	return 0
}

//export STConnect
func STConnect(c_conn C.int) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	if conn.Connect() != nil {
		return -1
	}
	return 0
}

//export STSendData
func STSendData(c_conn C.int, buffer *C.char, size C.int) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	conn.DataPack.Data = []byte(C.GoStringN(buffer, size))
	conn.Send()
	return 0
}

//export STReceive
func STReceive(c_conn C.int) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	conn.Receive()
	return 0
}

//export STReceiveData
func STReceiveData(c_conn C.int, buffer *C.char) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	conn.Receive()
	C.memcpy(unsafe.Pointer(buffer), unsafe.Pointer(C.CString(string(conn.DataPack.Data))), C.ulong(len(conn.DataPack.Data)))
	return C.int(len(conn.DataPack.Data))
}

//export STDataPackGetSize
func STDataPackGetSize(c_conn C.int) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	return C.int(conn.DataPack.Size)
}

//export STDataPackGetData
func STDataPackGetData(c_conn C.int, buffer *C.char) C.int {
	conn := conns[c_conn]
	if conn == nil {
		return -1
	}
	C.memcpy(unsafe.Pointer(buffer), unsafe.Pointer(C.CString(string(conn.DataPack.Data))), C.ulong(len(conn.DataPack.Data)))
	return C.int(len(conn.DataPack.Data))
}

//export STClose
func STClose(c_conn C.int) {
	conn := conns[c_conn]
	if conn == nil {
		return
	}
	conn.Close()
	conns[c_conn] = nil
}
