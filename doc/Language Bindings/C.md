## STProto language binging for C
### Functions
```C
stconn_t STNewConnection(const char *, uint16_t);
int STAccept(stconn_t);
int STConnect(stconn_t);
int STSendData(stconn_t, char *, size_t);
int STReceive(stconn_t);
size_t STReceiveData(stconn_t, char *);
size_t STDataPackGetSize(stconn_t);
int STDataPackGetData(stconn_t, char *);
void STClose(stconn_t);
```
#### STNewConnection
* Arg1: host address
* Arg2: host port
* Return: STProto connection object

It returns a STProto connection object.
#### STAccept
* Arg1: STProto connection object
* Return: if successful

Wait for a connection.
#### STConnect
* Arg1: STProto connection object
* Return: if successful

Connect to server.
#### STSend
* Arg1: STProto connection object
* Return: if successful

Send data.
#### STReceive
* Arg1: STProto connection object
* Arg2: buffer to place data
* Return: data size

Receive data and write the data into buffer.
#### STSendData
* Arg1: STProto connection object
* Arg2: data buffer
* Arg3: data size
* Return: if successful

Send data.
#### STDataPackGetSize
* Arg1: STProto connection object
* Return: data size

Get the data size of the received datapack.
#### STDataPackGetData
* Arg1: STProto connection object
* Arg2: buffer to place data
* Return: if successful

Get the data size of the received datapack.
#### STClose
* Arg1: STProto connection object
* Return: if successful

Close the connection.

### Constants
* DEFAULT_PORT: default port of STProto

### Example
`server.c`
```C
#include <stdio.h>
#include <stdlib.h>
#include "api/c/STProto.h"

int main()
{
	stconn_t conn = STNewConnection("localhost", DEFAULT_PORT);
	STAccept(conn);
	char *data = malloc(1024);
	size_t size = STReceiveData(conn, data);
	for(size_t i = 0; i < size; i++)
	{
		printf("%c", data[i]);
	}
	return 0;
}

```
`client.c`
```C
#include <string.h>
#include "api/c/STProto.h"

int main()
{
	stconn_t conn = STNewConnection("localhost", DEFAULT_PORT);
	STConnect(conn);
	char *data = "TEST";
	STSendData(conn, data, strlen(data));
	STClose(conn);
	return 0;
}
```
