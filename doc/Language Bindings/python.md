## STProto language binging for Python
### Classes
```python
class STProto:
    def __init__(self, host: str, port: int) -> None:
    def Accept(self) -> None:
    def Connect(self) -> None:
    def SendData(self, data: bytes) -> None:
    def ReceiveData(self) -> bytes:
    def Close(self) -> None:
```
#### Methods
##### \_\_init\_\_
* Arg1: host address
* Arg2: host port

Create a new STProto connection object.
##### Accept
Wait for a connection.
##### Connect
Connect to server.
##### SendData
* Arg1: data to be sent.

Send data.
##### ReceiveData
* Return: received data

Receive data.
##### Close
Close the connection.
